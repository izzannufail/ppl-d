from django.db import models

# Create your models here.
class test_model(models.Model):
    id = models.CharField(max_length=10, primary_key=True)
    char = models.CharField(max_length=3)
