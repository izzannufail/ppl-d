from django.urls import path
from init import views

urlpatterns = [
    path('', views.index, name='Index'),
    path('add/', views.db_test_view, name='db_test_view')
]