# Generated by Django 4.1.7 on 2023-02-17 07:17

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="test_model",
            fields=[
                (
                    "id",
                    models.CharField(max_length=10, primary_key=True, serialize=False),
                ),
                ("char", models.CharField(max_length=3)),
            ],
        ),
    ]
