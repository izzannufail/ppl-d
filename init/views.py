from django.shortcuts import render
from .forms import db_test_form
from .models import test_model

# Create your views here.
def index(request):
    return render(request, 'base.html')

def db_test_view(request):
    _dbTest = db_test_form(request.POST or None)
    if (request.method == 'POST' and _dbTest.is_valid()):
        _dbTest.save()

    response = {
        'form': _dbTest
    }

    return render(request, 'dbtest.html', response)