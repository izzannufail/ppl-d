from django import forms
from .models import test_model

class db_test_form(forms.ModelForm):
    id = forms.CharField(
        max_length=10
    )
    rand_char = forms.CharField(
        max_length=3
    )

    class Meta:
        model = test_model
        fields = [
            'id', 'char',
        ]